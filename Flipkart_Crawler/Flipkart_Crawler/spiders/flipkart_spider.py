# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
import time
from scrapy.item import Item, Field
from ..items import FlipkartCrawlerItem
from scrapy.loader import ItemLoader


class FlipkartSpiderSpider(scrapy.Spider):
    name = 'flipkart_spider'
    allowed_domains = ['flipkart.com']
    page_number = 1
    start_urls = ['https://www.flipkart.com/search?q=SPF50+sunscreen+cream&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off&page=1']

    # rules = [
    #     Rule(LinkExtractor(allow=r'search?q=+vaginal+wash&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off&page=[0-9]'),
    #         callback='parse', follow=True)
    # ]


    # def __init__(self):
    #     self.driver = webdriver.Chrome("/home/rishabh/Downloads/chromedriver")

    def parse(self, response):
        for link in response.css('._2cLu-l'):
            
            links=link.css('._2cLu-l ::attr(href)').get()
            full_link = "https://www.flipkart.com" + str(links)
            yield response.follow(full_link, callback=self.parse_next)


    def parse_next(self, response):


        for quote in response.xpath('//div[@class="_3e7xtJ"]'):
            loader = ItemLoader(item=FlipkartCrawlerItem(),selector=quote,response=response)
            loader.add_css('Title','._35KyD6::text')
            
            loader.add_css('Price','._3qQ9m1::text')
            loader.add_xpath('Image','//div[@class="_2_AcLJ"]/@style')
            loader.add_css('Seller_Name','#sellerName span::text')
            loader.add_css('Description','._1zZOAc::text')
            loader.add_css('General','._2RngUh+ ._2RngUh ._3YhLQA::text')
            loader.add_css('Frequently_bought_together','._2g95Wd ._2cLu-l::text')
            loader.add_css('Rating','._1i0wk8::text')
            loader.add_css('Comments','._2xg6Ul::text')
        
            
            # loader.add_css('Merchant','#merchant-info::text')
            # loader.add_css('Specification','#feature-bullets .a-list-item::text')
            # loader.add_css('Offers','#sopp_feature_div .a-list-item::text')
            # loader.add_css('Product_details','#feature-bullets .a-list-item::text')
            # loader.add_css('Product_description','#productDescription p::text')
            # loader.add_xpath('Customers_who_bought_this_item_also_bought','//div[@class="a-section"]/img/@alt')
            loader.add_xpath('Customer_reviews','//span[@class="_38sUEc"]/span/span/text()')
            # loader.add_css('Customer_comments','.a-text-bold span *::text')
            # loader.add_xpath('Sold_by','//div[@id="merchant-info"]/a[1]/text()')
            # loader.add_xpath('Fulfilled_by','//div[@id="merchant-info"]/a[2]/text()')


            
            
            
            

            yield loader.load_item()

        #url = "https://www.amazon.com/s?i=electronics-intl-ship&rh=n%3A%2116225009011&page="+str(AmazonSpiderSpider.page_number)+"&qid=1559737580&ref=lp_16225009011_pg_"+str(AmazonSpiderSpider.page_number)
        url = "https://www.flipkart.com/search?q=SPF50+sunscreen+cream&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off&page="+str(FlipkartSpiderSpider.page_number)
        #url= "https://www.flipkart.com/search?q=+vaginal+wash&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off&page="+str(FlipkartSpiderSpider.page_number)
        if FlipkartSpiderSpider.page_number<=10:
            FlipkartSpiderSpider.page_number += 1
            yield response.follow(url, callback=self.parse)
