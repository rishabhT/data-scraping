# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst, Join


class FlipkartCrawlerItem(scrapy.Item):

    Title = scrapy.Field(
        input_processor = MapCompose(str.strip),
        output_processor = TakeFirst()
    )
    Price = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Customer_reviews = scrapy.Field(input_processor = MapCompose(str.strip))

    Image = scrapy.Field()


    Seller_Name = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Description = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    General = scrapy.Field()

    Frequently_bought_together = scrapy.Field()

    Rating = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Comments = scrapy.Field()
