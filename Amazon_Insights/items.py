# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst, Join

class AmazonInsightsItem(scrapy.Item):
    Title = scrapy.Field(
        input_processor = MapCompose(str.strip),
        output_processor = TakeFirst()
    )
    Price = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    MRP = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Sales_Rank = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    
    Customer_reviews = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    
    Total_reviews = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Date = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Link = scrapy.Field()
