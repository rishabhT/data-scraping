# -*- coding: utf-8 -*-
import scrapy
# from scrapy.contrib.linkextractors import LinkExtractor
# from scrapy.contrib.spiders import CrawlSpider, Rule
import time
from scrapy.item import Item, Field
from ..items import AmazonInsightsItem
from scrapy.loader import ItemLoader


class SpiderSpider(scrapy.Spider):
    name = 'spider'
    page_number = 1
    allowed_domains = ['amazon.in']
    f = open("/home/rishabh/Downloads/link.txt", "r")
    g = f.read()
    g=g.split("\n")
    start_urls = g
    # rules = [
    #     Rule(LinkExtractor(allow=r's?k=vaginal+wash&page=[0-9]&qid=1559887925&ref=sr_pg_[0-9]'),
    #         callback='parse', follow=True)
    # ]


    # def __init__(self):
    #     self.driver = webdriver.Chrome("/home/rishabh/Downloads/chromedriver")

    # def parse(self, response):


    #     for link in response.xpath('//h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-2"]'):
            
    #         links=link.css('.a-link-normal.a-text-normal::attr(href)').get()
        
          
    #         full_link = "https://www.amazon.in" + str(links)
    #         yield response.follow(full_link, callback=self.parse_next)


    def parse(self, response):


        for quote in response.xpath('//div[@id="dp"]'):
            loader = ItemLoader(item=AmazonInsightsItem(),selector=quote,response=response)
            loader.add_xpath('Title','//div[@id="titleSection"]/h1/span/text()')
            loader.add_xpath('MRP','//td[@class="a-span12 a-color-secondary a-size-base"]/span[1]/text()')
            loader.add_xpath('Price','//td[@class="a-span12"]/span[1]/text()')
            loader.add_xpath('Sales_Rank','//span[@class="zg_hrsr_rank"]/text()')
            loader.add_xpath('Date','//tr[@class="date-first-available"]/td[2]/text()')
            # loader.add_css('Specification','#feature-bullets .a-list-item::text')
            # loader.add_css('Product_details','#feature-bullets .a-list-item::text')
            # loader.add_css('Product_description','#productDescription p::text')
            # loader.add_xpath('Customers_who_bought_this_item_also_bought','//div[@class="a-section"]/img/@alt')
            loader.add_css('Customer_reviews','.arp-rating-out-of-text::text')
            # loader.add_xpath('Customer_comments','//a[@class="a-size-base a-link-normal review-title a-color-base review-title-content a-text-bold"]/span/text()')
            # loader.add_xpath('Sold_by','//div[@id="merchant-info"]/a[1]/text()')
            # loader.add_xpath('Fulfilled_by','//div[@id="merchant-info"]/a[2]/text()')
            loader.add_xpath('Total_reviews','//span[@class="a-declarative"]/a/span/text()')
            loader.add_xpath('Link',"//link[@rel='canonical']/@href")
            yield loader.load_item()

        #url = "https://www.amazon.com/s?i=electronics-intl-ship&rh=n%3A%2116225009011&page="+str(AmazonSpiderSpider.page_number)+"&qid=1559737580&ref=lp_16225009011_pg_"+str(AmazonSpiderSpider.page_number)
        # url = "https://www.amazon.in/s?i=electronics&bbn=1805560031&rh=n%3A976419031%2Cn%3A976420031%2Cn%3A1389401031%2Cn%3A1389432031%2Cn%3A1805560031%2Cp_36%3A1500000-99999999&dc&page="+str(SpiderSpider.page_number)+"&fst=as%3Aoff&pf_rd_i=1389401031&pf_rd_m=A1K21FY43GMZF8&pf_rd_p=779446b2-92a2-4a73-845f-80ad54a148d4&pf_rd_r=Z0V1EJK7ZGHNT85X79JE&pf_rd_s=merchandised-search-2&pf_rd_t=101&qid=1561788292&rnid=3837712031&ref=sr_pg_"+str(SpiderSpider.page_number)
        # if SpiderSpider.page_number<=2:
        #     SpiderSpider.page_number += 1
        #     yield response.follow(url, callback=self.parse)