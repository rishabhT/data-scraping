# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


import psycopg2


class AmazonCrawlerPipeline(object):

    def open_spider(self, spider):
        hostname = 'localhost'
        username = 'postgres'
        password = 'postgres'
        database = 'amazon_data'
        self.connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
        self.cur = self.connection.cursor()

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    def process_item(self, item, spider):
        self.cur.execute("insert into amazon_content(title,price,merchant,image,specification,product_details,product_description,customers_who_bought_this_item_also_bought,customer_reviews,customer_comments,sold_by,fulfilled_by,total_reviews) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(item['Title'],item['Price'],item['Merchant'],item['Image'],item['Specification'],item['Product_details'],item['Product_description'],item['Customers_who_bought_this_item_also_bought'],item['Customer_reviews'],item['Customer_comments'],item['Sold_by'],item['Fulfilled_by'],item['Total_reviews']))
        self.connection.commit()
        return item