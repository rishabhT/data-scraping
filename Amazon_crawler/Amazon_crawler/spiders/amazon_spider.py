# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
import time
from scrapy.item import Item, Field
from ..items import AmazonCrawlerItem
from scrapy.loader import ItemLoader



class AmazonSpiderSpider(scrapy.Spider):
    name = 'amazon_spider'
    page_number = 1
    allowed_domains = ['amazon.in']
    start_urls = ['https://www.amazon.in/s?k=ortho+oil&page=1&qid=1562225234&ref=sr_pg_1']
    # rules = [
    #     Rule(LinkExtractor(allow=r's?k=vaginal+wash&page=[0-9]&qid=1559887925&ref=sr_pg_[0-9]'),
    #         callback='parse', follow=True)
    # ]


    # def __init__(self):
    #     self.driver = webdriver.Chrome("/home/rishabh/Downloads/chromedriver")

    def parse(self, response):


        for link in response.xpath('//h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-4"]'):
            
            links=link.css('.a-link-normal.a-text-normal::attr(href)').get()
        
          
            full_link = "https://www.amazon.in" + str(links)
            yield response.follow(full_link, callback=self.parse_next)


    def parse_next(self, response):


        for quote in response.xpath('//div[@class="a-container"]'):
            loader = ItemLoader(item=AmazonCrawlerItem(),selector=quote,response=response)
            loader.add_xpath('Title','//div[@id="titleSection"]/h1/span/text()')
            loader.add_xpath('Price','//td[@class="a-span12"]/span/text()')
            loader.add_xpath('Image','//div[@id="imgTagWrapperId"]/img/@data-a-dynamic-image')
            loader.add_css('Merchant','#merchant-info::text')
            loader.add_css('Specification','#feature-bullets .a-list-item::text')
            loader.add_css('Product_details','#feature-bullets .a-list-item::text')
            loader.add_css('Product_description','#productDescription p::text')
            loader.add_xpath('Customers_who_bought_this_item_also_bought','//div[@class="a-section"]/img/@alt')
            loader.add_css('Customer_reviews','.arp-rating-out-of-text::text')
            loader.add_xpath('Customer_comments','//a[@class="a-size-base a-link-normal review-title a-color-base review-title-content a-text-bold"]/span/text()')
            loader.add_xpath('Sold_by','//div[@id="merchant-info"]/a[1]/text()')
            loader.add_xpath('Fulfilled_by','//div[@id="merchant-info"]/a[2]/text()')
            loader.add_xpath('Total_reviews','//span[@class="a-declarative"]/a/span/text()')



            
            
            
            

            yield loader.load_item()

        #url = "https://www.amazon.com/s?i=electronics-intl-ship&rh=n%3A%2116225009011&page="+str(AmazonSpiderSpider.page_number)+"&qid=1559737580&ref=lp_16225009011_pg_"+str(AmazonSpiderSpider.page_number)
        url = "https://www.amazon.in/s?k=ortho+oil&page="+str(AmazonSpiderSpider.page_number)+"&qid=1562225234&ref=sr_pg_"+str(AmazonSpiderSpider.page_number)
        if AmazonSpiderSpider.page_number<=7:
            AmazonSpiderSpider.page_number += 1
            yield response.follow(url, callback=self.parse)




    
