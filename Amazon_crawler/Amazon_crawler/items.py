# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst, Join


class AmazonCrawlerItem(scrapy.Item):

    Title = scrapy.Field(
        input_processor = MapCompose(str.strip),
        output_processor = TakeFirst()
    )
    Price = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Merchant = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Image = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Specification = scrapy.Field()

    Product_details = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Product_description = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Customers_who_bought_this_item_also_bought = scrapy.Field()

    Customer_reviews = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Customer_comments = scrapy.Field()
    
    Sold_by = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())
    Fulfilled_by = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())
    
    Total_reviews = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())
