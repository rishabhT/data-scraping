# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html


import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst, Join


class NykaaItem(scrapy.Item):

    Title = scrapy.Field(
        input_processor = MapCompose(str.strip),
        output_processor = TakeFirst()
    )
    Price = scrapy.Field(Merchant = scrapy.Field(input_processor = MapCompose(str.strip),output_processor = TakeFirst()))

    # Merchant = scrapy.Field(input_processor = MapCompose(str.strip),
    #     output_processor = TakeFirst())

    Image = scrapy.Field()

    Features = scrapy.Field()
    Composition = scrapy.Field()
    How_to_use = scrapy.Field()

    Description = scrapy.Field()
    Ratings = scrapy.Field()
    Reviews = scrapy.Field()
