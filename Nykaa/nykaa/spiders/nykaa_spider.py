# -*- coding: utf-8 -*-\
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.item import Item, Field
from ..items import NykaaItem
from scrapy.loader import ItemLoader


class NykaaSpiderSpider(scrapy.Spider):
    name = 'nykaa_spider'
    allowed_domains = ['nykaa.com']
    start_urls = [
        "https://www.nykaa.com/st-botanica-vitamin-c-20-vitamin-e-hyaluronic-acid-facial-serum/p/269512?ptype=product&productId=269512&skuId=269512&pps=1",
        "https://www.nykaa.com/st-botanica-hyaluronic-acid-facial-serum-vitamin-c-e/p/269510?ptype=product&productId=269510&skuId=269510&pps=2",
        "https://www.nykaa.com/st-botanica-retinol-2-5-vitamin-e-hyaluronic-acid-professional-facial-serum/p/269511?ptype=product&productId=269511&skuId=269511&pps=3",
        "https://www.nykaa.com/nutroactive-vitamin-e-pure-concentrated/p/403045?ptype=product&productId=403045&skuId=403045&pps=4",
    ]

    def parse(self, response):
        loader = ItemLoader(item=NykaaItem(),selector=response,response=response)
        loader.add_xpath('Title','//div[@class="product-des__details-title"]/h1/text()')
        loader.add_xpath('Price','//div[@class="price-info"]/span[2]/span/text()[2]')
        loader.add_xpath('Image','//div[@class="post-card__img-wrap1 "]/div/img/@src')
        loader.add_xpath('Ratings','//div[@class="m-content__product-list__ratings js-rating-count-popup"]/span[2]/text()')
        loader.add_xpath('Description','//div[@class="pdp-description-tab-item "]/div/p/text()')
        loader.add_xpath('Reviews','//div[@class="product-des__details-div scroll-to-target pull-left"]/span[3]/text()')
        yield loader.load_item()


    #     for link in response.xpath('//div[@class="product-list-box card desktop-cart"]'):
            
    #         links=link.xpath('//div[@class="product-list-box card desktop-cart"]/a/@href').get()
    #         full_link = "https://www.nykaa.com" + str(links)
    #         yield response.follow(full_link, callback=self.parse_next)
    
    # def parse_next(self, response):
    #     for quote in response.xpath('//div[@class="row position-relative"]'):
    #         loader = ItemLoader(item=NykaaItem(),selector=quote,response=response)
    #         loader.add_xpath('Title','//h1["product-title"]/text()')
    #         loader.add_css('Price','.product-des__details .post-card__content-price-offer::text')
    #         loader.add_xpath('Image','//div[@class="post-card__img-wrap1"]/div/img/@src')
    #         loader.add_css('Seller_Name','.sold-by::text')
    #         loader.add_css('Description','.pdp-description-tab-item p:nth-child(1)::text')
    #         loader.add_css('Frequently_bought_together','.m-content__product-list__title span::text')
    #         loader.add_xpath('Rating','//div[@class ="average-rating-star-icon"]/text()')
    #         loader.add_css('Comments','h4::text')
    #         yield loader.load_item()
