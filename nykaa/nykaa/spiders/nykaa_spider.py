# -*- coding: utf-8 -*-\
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.item import Item, Field
from ..items import NykaaItem
from scrapy.loader import ItemLoader
from selenium import webdriver
from scrapy.selector import Selector
from scrapy.http import Request
import time




class NykaaSpiderSpider(scrapy.Spider):
    name = 'nykaa_spider'
    allowed_domains = ['nykaa.com']


    def start_requests(self):
        self.driver = webdriver.Chrome('/home/rishabh/Downloads/chromedriver')
        self.driver.get(r"https://www.nykaa.com/search/result/?ptype=search&q=vatamin%20e%20face%20serum&root=search&sourcepage=search&searchType=Manual")
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(20)
        sel = Selector(text=self.driver.page_source)
        
        links = sel.xpath('//div[@class="product-list-box card desktop-cart"]/a/@href').extract()
        for link in links:
            url = "https://www.nykaa.com"+str(link)
            yield Request(url,callback=self.parse)

        # while True:
        #     try:
        #         driver.find_element_by_xpath('//button[@class="view-more-btn common-btn"]').click()
        #     except:
        #         pass      


    def parse(self, response):
        for quote in response.xpath('//div[@class="m-content desktop product-description"]'):
            loader = ItemLoader(item=NykaaItem(),selector=quote,response=response)
            loader.add_xpath('Title','//div[@class="product-des__details-title"]/h1/text()')
            loader.add_xpath('Price','//div[@class="price-info"]/span[2]/span/text()[2]')
            loader.add_xpath('Image','//div[@class="post-card__img-wrap1 "]/div/img/@src')
            loader.add_xpath('Ratings','//div[@class="m-content__product-list__ratings js-rating-count-popup"]/span[2]/text()')
            loader.add_xpath('Description','//div/p[1]/text()')
            loader.add_xpath('Reviews','//div[@class="product-des__details-div scroll-to-target pull-left"]/span[3]/text()')
            yield loader.load_item()


    #     for link in response.xpath('//div[@class="product-list-box card desktop-cart"]'):
            
    #         links=link.xpath('//div[@class="product-list-box card desktop-cart"]/a/@href').get()
    #         full_link = "https://www.nykaa.com" + str(links)
    #         yield response.follow(full_link, callback=self.parse_next)
    
    # def parse_next(self, response):
    #     for quote in response.xpath('//div[@class="row position-relative"]'):
    #         loader = ItemLoader(item=NykaaItem(),selector=quote,response=response)
    #         loader.add_xpath('Title','//h1["product-title"]/text()')
    #         loader.add_css('Price','.product-des__details .post-card__content-price-offer::text')
    #         loader.add_xpath('Image','//div[@class="post-card__img-wrap1"]/div/img/@src')
    #         loader.add_css('Seller_Name','.sold-by::text')
    #         loader.add_css('Description','.pdp-description-tab-item p:nth-child(1)::text')
    #         loader.add_css('Frequently_bought_together','.m-content__product-list__title span::text')
    #         loader.add_xpath('Rating','//div[@class ="average-rating-star-icon"]/text()')
    #         loader.add_css('Comments','h4::text')
    #         yield loader.load_item()
