# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst, Join


class W1MgCrawlerItem(scrapy.Item):
    Title = scrapy.Field(
        input_processor = MapCompose(str.strip),
        output_processor = TakeFirst()
    )
    Price = scrapy.Field()

    Merchant = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Image = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Specification = scrapy.Field()


    Customers_who_bought_this_item_also_bought = scrapy.Field()

   