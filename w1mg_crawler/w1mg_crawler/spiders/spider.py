# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
import time
from scrapy.item import Item, Field
from ..items import W1MgCrawlerItem
from scrapy.loader import ItemLoader


class SpiderSpider(scrapy.Spider):
    name = 'spider'
    allowed_domains = ['1mg.com']
    start_urls = ['https://www.1mg.com/search/all?name=vaginal%20wash&filter=true&state=0&scroll_id=Bq2QNcz0ep38Pisf2Mzgng==']
    page_number = 0
    rules = [
        Rule(LinkExtractor(allow=r'search/all?name=vaginal%20wash&filter=true&state=[0-9]&scroll_id=Bq2QNcz0ep38Pisf2Mzgng=='),
            callback='parse', follow=True)
    ]

    def parse(self, response):


        for link in response.xpath('//div[@class = "style__product-box___3oEU6"]'):
            
            links=link.css('.style__product-link___1hWpa::attr(href)').get()
        
          
            full_link = "https://www.1mg.com" + str(links)
            yield response.follow(full_link, callback=self.parse_next)


    def parse_next(self, response):


        for quote in response.xpath('//div[@class="otc-container"]'):
            loader = ItemLoader(item=W1MgCrawlerItem(),selector=quote,response=response)
            loader.add_css('Title','.ProductTitle__product-title___3QMYH::text')
            loader.add_xpath('Price','//div[@class="PriceDetails__discount-div___nb724"]/text()')
            loader.add_xpath('Image','//div[@class="col-xs-10 ProductImage__preview-container___2oTeX"]/div/img/@src')
            loader.add_xpath('Merchant','//div[@class="ProductTitle__manufacturer___sTfon"]/a/text()')
            loader.add_xpath('Customers_who_bought_this_item_also_bought','//div[@class="style__name___3YOZc style__large-font___2dBUf"]/text()')



            
            
            
            

            yield loader.load_item()
        #url = "https://www.1mg.com/search/all?name=vaginal%20wash&filter=true&state=0&scroll_id=Bq2QNcz0ep38Pisf2Mzgng=="
        #url = "https://www.amazon.com/s?i=electronics-intl-ship&rh=n%3A%2116225009011&page="+str(AmazonSpiderSpider.page_number)+"&qid=1559737580&ref=lp_16225009011_pg_"+str(AmazonSpiderSpider.page_number)
        url = "https://www.1mg.com/search/all?name=vaginal%20wash&filter=true&state="+str(SpiderSpider.page_number)+"&scroll_id=Bq2QNcz0ep38Pisf2Mzgng=="
        if SpiderSpider.page_number<=1:
            SpiderSpider.page_number += 1
            yield response.follow(url, callback=self.parse)