import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from time import sleep
from scrapy.loader import ItemLoader
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from scrapy.selector import Selector
from scrapy.http import Request,FormRequest
import pandas as pd
from pandas import ExcelWriter
from selenium.webdriver.chrome.options import Options

def append_df_to_excel(df, excel_path):
    df_excel = pd.read_excel(excel_path)
    result = pd.concat([df_excel, df], ignore_index=True)
    result.to_excel(excel_path, index=False)

class ToflerSpider(scrapy.Spider):
    name = 'tofler'
    page_no = 1
    allowed_domains = ['tofler.in']
    start_urls = ['https://www.tofler.in/login/']


    

    def start_requests(self):

        options = Options()
        options.add_argument('--proxy-server=180.183.101.200:8080')
        self.driver = webdriver.Chrome(executable_path=r"/home/rishabh/Downloads/chromedriver", chrome_options=options)
        self.driver.get("https://www.tofler.in/login")

        # self.driver = webdriver.Chrome(r"/home/rishabh/Downloads/chromedriver")
        # self.driver.get("https://www.tofler.in/login")
        sleep(0.5)
        username = self.driver.find_element_by_xpath('//form[@id="regularloginform"]/div[1]/input')
        sleep(0.5)
        password = self.driver.find_element_by_xpath('//form[@id="regularloginform"]/div[2]/input')
        sleep(0.5)
        username.send_keys("jcz42267@bcaoo.com")
        sleep(0.5)
        password.send_keys("jcz42267")
        sleep(0.5)
        submit=self.driver.find_element_by_xpath('//input[@id="signinbutton"]')
        submit.click()
        sleep(2)       
        Name = []
        Status = []
        Overview= []
        CIN = []
        Incorporation_date = []
        Last_report_agm_date = []
        Authorized_capital = []
        Paidup_capital = []
        Industry = []
        Type = []
        Category = []
        Subcategory = []
        Email_address = []
        Website = []
        Registered_address = []
        Directors = []
 
        
        for i in range(140,2101):
            href = []
            self.driver.get("https://www.tofler.in/companylist/telangana/pg-"+str(i))
            sleep(8)
            links = self.driver.find_elements_by_xpath('//td/a[@class="complink"]')
            for link in links:
                href.append(link.get_attribute('href'))

            for url in href:
                self.driver.get(url)
                sleep(5)
                sel = Selector(text=self.driver.page_source)

                try:
                    Name.append(sel.xpath('//div[@class="page-headers"]/h1/text()').extract_first())
                except:
                    Name.append("None")


                try:
                    Status.append(sel.xpath('//span[@class="large-text"]/text()').extract_first())
                except:
                    Status.append("None")


                try:
                    Overview.append(sel.css('#overview p::text').extract())
                except:
                    Overview.append("None")

                try:
                    CIN.append(sel.css('.clearfix+ .row .col:nth-child(1) p::text').extract_first())
                except:
                    CIN.append("None") 
                try:
                    Incorporation_date.append(sel.css('.clearfix+ .row .col:nth-child(2) p::text').extract_first())
                except:
                    Incorporation_date.append("None") 

                try:
                    Last_report_agm_date.append(sel.css('.clearfix+ .row .col~ .col+ .col p::text').extract_first())
                except:
                    Last_report_agm_date.append("None")    

                try:
                    Authorized_capital.append(sel.css('.row:nth-child(6) .col:nth-child(1) p::text').extract_first())
                except:
                    Authorized_capital.append("None") 

                try:
                    Paidup_capital.append(sel.css('.row:nth-child(6) .col:nth-child(2) p::text').extract_first())
                except:
                    Paidup_capital.append("None")

                try:
                    Industry.append(sel.css('.row:nth-child(6) .col~ .col+ .col p::text').extract_first())
                except:
                    Industry.append("None")  

                try:
                    Type.append(sel.css('.row:nth-child(8) .col:nth-child(1) p::text').extract_first())
                except:
                    Type.append("None")  

                try:
                    Category.append(sel.css('.row:nth-child(8) .col:nth-child(2) p::text').extract_first())
                except:
                    Category.append("None")

                try:
                    Subcategory.append(sel.css('.row:nth-child(8) .col~ .col+ .col p::text').extract_first())
                except:
                    Subcategory.append("None") 

                try:
                    Email_address.append(sel.css('.m12:nth-child(1) p:nth-child(2)::text').extract_first())
                except:
                    Email_address.append("None")

                try:
                    Registered_address.append(sel.css('.col+ .m12 p::text').extract_first())
                except:
                    Registered_address.append("None")

                try:
                    Website.append(sel.css('.modal-trigger::text').extract_first())
                except:
                    Website.append("None")

                try:
                    Directors.append(sel.css('#directors-time_line-view .waves-dark::text').extract())
                except:
                    Directors.append("None")
            df=pd.DataFrame(list(zip(Name,Status,Overview,CIN,Incorporation_date,Last_report_agm_date,Authorized_capital,Paidup_capital,Industry,Type,Category,Subcategory,Email_address,Registered_address,Website,Directors)),columns=['Name','Status','Overview','CIN','Incorporation_date','Last_report_agm_date','Authorized_capital','Paidup_capital','Industry','Type','Category','Subcategory','Email_address','Registered_address','Website','Directors'])          
            append_df_to_excel(df,"telangana_1.xlsx")

        # df=pd.DataFrame(list(zip(Name,Status,Overview,CIN,Incorporation_date,Last_report_agm_date,Authorized_capital,Paidup_capital,Industry,Type,Category,Subcategory,Email_address,Registered_address,Website,Directors)),columns=['Name','Status','Overview','CIN','Incorporation_date','Last_report_agm_date','Authorized_capital','Paidup_capital','Industry','Type','Category','Subcategory','Email_address','Registered_address','Website','Directors'])          
        # writer = ExcelWriter('Telangana_tofler.xlsx')
        # df.to_excel(writer,'Sheet5')
        # writer.save()   



