# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst, Join


class LinkedinCrawlerItem(scrapy.Item):

    Name = scrapy.Field(
        input_processor = MapCompose(str.strip),
        output_processor = TakeFirst()
    )
    Designation = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Company_name = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    College = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    About = scrapy.Field()

    Experience_designation = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Experience_company = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())


