import scrapy
# from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from time import sleep
# from scrapy.item import Item, Field
# from ..items import LinkedinCrawlerItem
from scrapy.loader import ItemLoader
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from scrapy.selector import Selector
from scrapy.http import Request
import pandas as pd
from pandas import ExcelWriter

class LinkedinSpiderSpider(scrapy.Spider):
    name = 'linkedin'
    page_number = 1
    allowed_domains = ['linkedin.com']


    def start_requests(self):

        self.driver = webdriver.Chrome(r"/home/rishabh/Downloads/chromedriver")
        self.driver.get("https://www.linkedin.com/login?trk=guest_homepage-basic_nav-header-signin")
        sleep(0.5)
        username = self.driver.find_element_by_id("username")
        sleep(0.5)
        password = self.driver.find_element_by_id("password")
        sleep(0.5)
        username.send_keys("rasulm883@gmail.com")
        sleep(0.5)
        password.send_keys("0ptimu$prime")
        sleep(0.5)
        submit=self.driver.find_element_by_xpath('//div[@class="login__form_action_container "]/button')
        submit.click()
        sleep(2)
        self.driver.get("https://www.google.com/")
        sleep(0.5)
        google = self.driver.find_element_by_xpath('//div[@class="a4bIc"]/input')
        google.send_keys('site:linkedin.com/in/ AND "python developer" AND "noida"')
        sleep(5)
        google.send_keys(Keys.RETURN)
        sleep(2)

        linkedin_url = self.driver.find_elements_by_tag_name("cite")
        linkedin_url = [url.text for url in linkedin_url]
        Name= []
        Designation=[]
        Company_name = []
        Company_name=[]
        College = []
        About  = []
        Experience_designation= []
        Experience_company = []


        for url in linkedin_url:
            self.driver.get(url)
            sleep(5)
            sel = Selector(text=self.driver.page_source)

            try:
                Name.append(sel.xpath('//li[@class="inline t-24 t-black t-normal break-words"]/text()').extract())
            except:
                Name.append("None")


            try:
                Designation.append(sel.xpath('//h2[@class="mt1 t-18 t-black t-normal"]/text()').extract())
            except:
                Designation.append("None")


            try:
                Company_name.append(sel.xpath('//span[@id="ember87"]/text()').extract())
            except:
                Company_name.append("None")


            try:
                College.append(sel.xpath('//span[@id="ember90"]/text()').extract())
            except:
                College.append("None") 

            try:
                About.append(sel.xpath('//p[@id="ember114"]/span/text()').extract())
            except:
                About.append("None") 

            try:
                Experience_designation.append(sel.xpath('//div[@class="display-flex flex-column full-width"]/a/div[2]/h3/text()').extract())
            except:
                Experience_designation.append("None")    

            try:
                Experience_company.append(sel.xpath('//div[@class="display-flex flex-column full-width"]/a/div[2]/h4/span[2]/text()').extract())
            except:
                Experience_company.append("None") 

        df=pd.DataFrame(list(zip(Name,Designation,Company_name,College,About,Experience_designation,Experience_company)),columns=['Name','Designation','Company_name','College','About','Experience_designation','Experience_company'])          
        

        writer = ExcelWriter('Linkedin_Data.xlsx')
        df.to_excel(writer,'Sheet5')
        writer.save()   
