# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
import time
from scrapy.item import Item, Field
from ..items import HelthkartCrawlerItem
from scrapy.loader import ItemLoader


class HealthkartSpider(scrapy.Spider):
    name = 'healthkart'
    start_urls = ['https://www.healthkart.com/search?txtQ=vaginal+wash']
    rules = [
        Rule(LinkExtractor(allow=r'search?txtQ=vaginal+wash[0-9]'),
            callback='parse', follow=True)
    ]
    def parse(self, response):
        for link in response.xpath('//div[@class="hk-variants-box-container"]'):
            links=link.xpath('//a[@class="variant-link"]/@href').get()
            full_link = "https://www.healthkart.com" + str(links)
            yield response.follow(full_link, callback=self.parse_next)


    def parse_next(self, response):


        for quote in response.xpath('//div[@class="HK-pagecontainer"]'):
            loader = ItemLoader(item=HelthkartCrawlerItem(),selector=quote,response=response)
            loader.add_xpath('Title','//div[@class="hk-packproductcontent"]/h1/text()')
            loader.add_xpath('Price','//div[@class="price-container"]/span/text()')
            loader.add_xpath('Image','//div[@class="img-zoom-sec"]/div[2]/img/@src')
            loader.add_xpath('Merchant','//div[@class="product-info"]/div/span[2]/text()')
            loader.add_xpath('Product_Info','//div[@class="product-key-points"]/ul/li/span/text()')
            yield loader.load_item()
