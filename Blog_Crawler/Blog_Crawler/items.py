# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose,TakeFirst, Join


class BlogCrawlerItem(scrapy.Item):
    Title = scrapy.Field(
        input_processor = MapCompose(str.strip),
        output_processor = TakeFirst()
    )
    Author = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Published = scrapy.Field(input_processor = MapCompose(str.strip),
        output_processor = TakeFirst())

    Comments = scrapy.Field()

    Blog_Text = scrapy.Field()

