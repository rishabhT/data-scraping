# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
import time
from scrapy.item import Item, Field
from ..items import BlogCrawlerItem
from scrapy.loader import ItemLoader


class BlogSpiderSpider(scrapy.Spider):
    name = 'blog_spider'
    start_urls = ['https://www.womensweb.in/articles/health-and-hygiene-tips/']
    

    def parse(self, response):
        loader = ItemLoader(item=BlogCrawlerItem(),selector=response,response=response)
        loader.add_xpath('Title','//h1[@class="sinlge-artivcal-title float-left"]/text()')
        loader.add_xpath('Author','//h4[@class="author_name"]/a/text()')
        loader.add_xpath('Published','//div[@class="text-right"]/text()')
        loader.add_xpath('Blog_Text','//div[@class="single-artical-content col-sm-12 clearfix"]/p/text()')
        loader.add_xpath('Comments','//div[@class="comment-body"]/p/text()')
    
        yield loader.load_item()