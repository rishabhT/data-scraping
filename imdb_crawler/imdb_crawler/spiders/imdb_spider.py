# -*- coding: utf-8 -*-
import scrapy
from imdb_crawler.items import MovieItem


class ImdbSpiderSpider(scrapy.Spider):
    name = 'imdb_spider'
    allowed_domains = ['imdb.com']
    start_urls = ('https://www.imdb.com/chart/top?ref_=nv_mv_250',)

    def parse(self, response):
        links = response.xpath('//*/td[@class="titleColumn"]/a/@href').extract()
        i = 1
        for link in links:
            abs_url = response.urljoin(link)
            url_next ="//*[@id='main']/div/span/div/div/div[2]/table/tbody/tr['+str(i)+']/td[3]/strong/text()"
            rating = response.xpath(url_next).extract()
            if (i <= len(links)):
                i = i+1
                yield scrapy.Request(abs_url,callback= self.parse_indetail,meta={'rating':rating})

    def parse_indetail(self,response):
        item = MovieItem()

        item['title'] = response.xpath('//*[@class="title_wrapper"]/h1/text()').extract_first()
        #item['directors'] = response.xpath('//*/div[@class="credit_summary_item"]/a/text()').extract_first()
        item['directors'] = response.xpath('//div[@class="plot_summary "]/div[2]/a/text()').extract_first()
        item['writers']  = response.xpath('//div[@class="plot_summary "]/div[3]/a/text()').extract()
        item['stars']  = response.xpath('//div[@class="plot_summary "]/div[4]/a/text()').extract()
        return item